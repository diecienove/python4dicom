#!/usr/bin/env python
__author__ = "daniele"
__version__ = "0.0.1"
__email__ = "diecienove@gmail.com"

import pydicom
import numpy as np
import os
import matplotlib.pyplot as plt

pathFolder='...'
dicomfiles=os.listdir(pathFolder)
dicomfiles.sort()

#create list to append images matrix
imgList=[]

#load dicom files and fill the images list
for dicomfile in dicomfiles:
    imgMatrix=pydicom.read_file(pathFolder+os.sep+dicomfile).pixel_array

    imgList.append(imgMatrix)

#plot the results
plt.figure("slices_grid")
ncols=8
nrows=len(imgList)//ncols
if (len(imgList)%ncols)>0:
    nrows+=1
for i in range (len(imgList)):
    plt.subplot(nrows,ncols,i+1)
    plt.imshow(imgList[i],cmap='gray',clim=[-400,600])

plt.show()